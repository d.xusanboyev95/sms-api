package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"sms-api/api/docs"
	"sms-api/internal/config"
	"sms-api/internal/pkg/cors"
	"sms-api/internal/pkg/logger"
	"sms-api/internal/rest"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"github.com/sethvargo/go-envconfig"
)

func main() {
	quitSignal := make(chan os.Signal, 1)
	signal.Notify(quitSignal, os.Interrupt)
	_ = godotenv.Load()

	var cfg config.Config
	if err := envconfig.ProcessWith(context.TODO(), &cfg, envconfig.OsLookuper()); err != nil {
		panic(fmt.Sprintf("envconfig.Process: %s", err))
	}

	fmt.Println(cfg.HTTPPort, "sa")
	docs.SwaggerInfo.Host = cfg.ServerIP + cfg.HTTPPort
	docs.SwaggerInfo.Description = "All references information to use in web and mobile"
	docs.SwaggerInfo.Schemes = []string{"http"}

	log := logger.New(cfg.LogLevel, "sms sevice")

	router := gin.Default()
	router.Use(cors.CORSMiddleware())

	srv := rest.New(router, cfg, log)

	restserver := &http.Server{
		Addr:    cfg.HTTPPort,
		Handler: srv,
	}

	go func() {
		if err := restserver.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatal(err.Error())
		}
	}()
	log.Info(fmt.Sprintf("Server started at %v", cfg.HTTPPort))

	// Wait for Ctrl^C
	const valtime = 5
	OSCall := <-quitSignal
	ctx, cancel := context.WithTimeout(context.Background(), valtime*time.Second)
	defer cancel()
	fmt.Printf("system call:%+v", OSCall)
	if err := restserver.Shutdown(ctx); err != nil {
		log.Error(fmt.Sprintf("REST Server Graceful Shutdown Failed: %s\n", err))
	}
	log.Info("REST Server Gracefully Shut Down")
}
