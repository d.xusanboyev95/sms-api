package usecase

import (
	"context"
	"sms-api/internal/config"
	"sms-api/internal/models"
	"sms-api/internal/pkg/logger"
)

type ISmsApi interface {
	SendSms(ctx context.Context, from, to, phoneNumber string) error
}

type SmsApiUsecase struct {
	smsApi ISmsApi
	log    logger.Logger
	cfg    config.Config
}

func New(sms ISmsApi, log logger.Logger, cfg config.Config) *SmsApiUsecase {
	return &SmsApiUsecase{
		smsApi: sms,
		log:    log,
		cfg:    cfg,
	}
}

func (uc *SmsApiUsecase) SendSms(ctx context.Context, data models.Sms) error {
	var logMsg = "uc.Analog.ProductStore "

	if err := uc.smsApi.SendSms(ctx, data.From, data.To, data.Content); err != nil {
		uc.log.Error(logMsg+"a.analog.ProductStore", logger.Error(err))

		return err
	}

	return nil
}
