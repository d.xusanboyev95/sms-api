package rest

import (
	"sms-api/internal/models"

	"github.com/gin-gonic/gin"
)

// sendSms godoc
// @Router /send/sms [POST]
// @Summary Send Sms
// @Description API Send Sms
// @Tags Sms
// @Produce json
// @Param request body models.Sms true "Sms details"
// @Success 201 {object} Response{data}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) sendSms() gin.HandlerFunc {
	return func(c *gin.Context) {
		var data models.Sms

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, nil, err)
			return

		}

		if err := s.SmsApiU.SendSms(c, data); err != nil {
			Return(c, "sms is failed", err)
			return
		}

		Return(c, "sms is sent successfully", nil)
	}
}
