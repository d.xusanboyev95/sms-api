package rest

import (
	"net/http"
	"sms-api/internal/config"
	"sms-api/internal/pkg/logger"
	"sms-api/internal/server"
	"sms-api/internal/usecase"

	"github.com/gin-gonic/gin"
)

type Server struct {
	router *gin.Engine
	log    logger.Logger
	cfg    config.Config

	SmsApiU usecase.SmsApiUsecase
	SmsApiI server.SmsApiServer
}

func New(router *gin.Engine, cfg config.Config, log logger.Logger) *Server {
	srv := &Server{
		router: router,
		log:    log,
		cfg:    cfg,
	}

	srv.SmsApiI = *server.New(srv.cfg, srv.log)
	srv.SmsApiU = *usecase.New(&srv.SmsApiI, srv.log, srv.cfg)

	srv.Router()

	return srv
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}
