package server

import (
	"context"
	"sms-api/internal/config"
	"sms-api/internal/pkg/logger"

	"github.com/NdoleStudio/httpsms-go"
)

type SmsApiServer struct {
	cfg    config.Config
	log    logger.Logger
	client *httpsms.Client
}

func New(cfg config.Config, log logger.Logger) *SmsApiServer {

	clientSms := httpsms.New(httpsms.WithAPIKey("sms api key"))

	return &SmsApiServer{
		cfg:    cfg,
		log:    log,
		client: clientSms,
	}
}

func (api *SmsApiServer) SendSms(ctx context.Context, from, to, content string) error {
	if _, _, err := api.client.Messages.Send(ctx, &httpsms.MessageSendParams{
		Content: content,
		From:    from,
		To:      to,
	}); err != nil {
		api.log.Error("something is wrong")
		return err
	}

	return nil
}
