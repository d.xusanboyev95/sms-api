package models

type Sms struct {
	From    string `json:"from"`
	To      string `json:"to"`
	Content string `json:"content"`
}
